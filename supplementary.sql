--Supplementary Activity 1
--Using the record db from yesterday's session
--Get all songs from the album "24K Magic" that are of the genre "Funk"
SELECT *
FROM songs s
  JOIN albums a ON s.album_id = a.id
WHERE s.genre LIKE "%funk%";
--Get the album title and artist name for all songs with a length less than 3 minutes that were released after 2015 
SELECT a.album_title,
  ar.name
FROM songs s
  JOIN albums a ON s.album_id = a.id
  JOIN artists ar ON a.artist_id = ar.id
WHERE TIME_TO_SEC(s.length) < 180
  AND a.date_released > '2015-12-31';
--Get all artists and their albums where the artist name ends with the letter "o" and the album was released before 2013 
SELECT *
FROM artists ar
  JOIN albums a ON ar.id = a.artist_id
WHERE (
    ar.name LIKE "%o %"
    OR ar.name LIKE "%o"
  )
  AND a.date_released < '2013-01-01';
--Get all songs with a length between 3 and 4 minutes from albums released by Taylor Swift
SELECT *
FROM songs s
  JOIN albums a ON s.album_id = a.id
  JOIN artists ar ON a.artist_id = ar.id
WHERE TIME_TO_SEC(s.length) BETWEEN 180 AND 240
  AND ar.name = 'Taylor Swift';
--Get all albums and their release dates for which the album title contains the word "born" and the artist name starts with the letter "J" 
SELECT a.album_title,
  a.date_released
FROM albums a
  JOIN artists ar ON a.artist_id = ar.id
WHERE a.album_title LIKE "%born%"
  AND ar.name LIKE "J%";
--Get the names of all songs by artists whose name starts with 'B' .
SELECT s.song_name
FROM artists ar
  JOIN albums a ON ar.id = a.artist_id
  JOIN songs s ON a.id = s.album_id
WHERE ar.name LIKE "B%"
  OR ar.name LIKE "% B%";
--Get the names of all songs longer than 4 minutes
SELECT s.song_name
FROM songs s
WHERE TIME_TO_SEC(s.length) > 240;