-- MySQL S5 Activity:
-- 1. Return the customerName of the customers who are from the Philippines
SELECT c.customerName
FROM customers c
WHERE c.country = "Philippines";
-- 2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"
SELECT c.contactLastName,
  c.contactFirstName
FROM customers c
WHERE c.customerName = "La Rochelle Gifts";
-- 3. Return the product name and MSRP of the product named "The Titanic"
SELECT p.productName,
  p.MSRP
FROM products p
WHERE p.productName = "The Titanic";
-- 4. Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"
SELECT e.firstName,
  e.lastName
FROM employees e
WHERE e.email = "jfirrelli@classicmodelcars.com";
-- 5. Return the names of customers who have no registered state
SELECT c.customerName
FROM customers c
WHERE c.state IS NULL;
-- 6. Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve
SELECT e.firstName,
  e.lastName,
  e.email
FROM employees e
WHERE e.lastName = "Patterson"
  AND e.firstName = "Steve";
-- 7. Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000
SELECT c.customerName,
  c.country,
  c.creditLimit
FROM customers c
WHERE c.country != "USA"
  AND c.creditLimit > 3000;
-- 8. Return the customer numbers of orders whose comments contain the string 'DHL'
SELECT o.customerNumber
FROM orders o
WHERE o.comments LIKE "%DHL%";
-- 9. Return the product lines whose text description mentions the phrase 'state of the art'
SELECT pl.productLine
FROM productLines pl
WHERE pl.textDescription LIKE "%state of the art%";
-- 10. Return the countries of customers without duplication
SELECT DISTINCT c.country
FROM customers c;
-- 11. Return the statuses of orders without duplication
SELECT DISTINCT o.status
FROM orders o;
-- 12. Return the customer names and countries of customers whose country is USA, France, or Canada
SELECT c.customerName,
  c.country
FROM customers c
WHERE c.country IN ("USA", "France", "Canada");
-- 13. Return the first name, last name, and office's city of employees whose offices are in Tokyo
SELECT e.firstName,
  e.lastName,
  of.city
FROM employees e
  JOIN offices of ON e.officeCode = of.officeCode
WHERE of.city = "Tokyo";
-- 14. Return the customer names of customers who were served by the employee named "Leslie Thompson"
SELECT c.customerName
FROM customers c
  JOIN employees e ON c.salesRepEmployeeNumber = e.employeeNumber
WHERE e.firstName = "Leslie"
  AND e.lastName = "Thompson";
-- 15. Return the product names and customer name of products ordered by "Baane Mini Imports"
SELECT p.productName,
  c.customerName
FROM customers c
  JOIN orders o ON c.customerNumber = o.customerNumber
  JOIN orderdetails od ON o.orderNumber = od.orderNumber
  JOIN products p ON od.productCode = p.productCode
WHERE c.customerName = "Baane Mini Imports";
-- 16. Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are in the same country
SELECT e.firstName,
  e.lastName,
  c.customerName,
  of.country
FROM employees e
  JOIN customers c ON e.employeeNumber = c.salesRepEmployeeNumber
  JOIN offices of ON e.officeCode = of.officeCode
WHERE c.country = of.country;
-- 17. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000
SELECT p.productName,
  p.quantityInStock
FROM products p
WHERE p.productLine = 'planes'
  AND p.quantityInStock < 1000;
-- 18. Show the customer's name with a phone number containing "+81".
SELECT c.customerName,
  c.phone
FROM customers c
WHERE c.phone LIKE '%+81%';